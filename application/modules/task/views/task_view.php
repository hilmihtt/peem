<style>
	a,a:focus {
		color: #333;
		transition: all .3s ease-in-out;
		text-decoration: none;
	}
	a:hover {
		color: #3a3;
		transition: all .3s ease-in-out;
		text-decoration: none;
	}
	.panel-toggle-content-list{
		display: none ;
	}
	.panel-toggle-list{
		cursor: auto;
	}
	#option {
		margin-top: 20px;
	}
</style>

<section id="option">
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">

						<span class="pull-right">
							<a href="javascript:;" onclick="showModal()" class="btn btn-primary  data-toggle="modal" data-target="#createTugas""> + Tambah Tugas </a>
							<a href="javascript:;" class="btn btn-default showAll">Buka Panel</a>
							<a href="javascript:;" class="btn btn-default closeAll">Tutup Panel</a>
						</span>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section id="task">
	<div class="container-fluid">
		<div class="row">
			
			<!-- Content 1-->
			<div class="col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="text-center">
							<h4>Daftar Tugas</h4>
						</div>
						<div class="panel-body">
							<!-- <table class="table table-stripted table-hover table-responsive">
								<thead>
									<th></th>
								</thead>
							</table> -->
							<div class="row">
								<?php if ($resultList == true) : ?>
								<?php $i = 1; foreach($resultList as $res): ?>
								<div class="panel panel-default">
									<a href="javascript:;">
										<div class="panel-body panel-toggle-list" id="panel-toggle-list-<?= $res->task_id?>">
											<?= $i ?>.
											<?= $res->title?>
											/ <?= $res->description ?>
											/ <?= $res->info ?>
										</div>
									</a>
									<div class="panel-footer panel-toggle-content-list" id="panel-toggle-content-list-<?= $res->task_id?>">
										<div class="row">
											<div class="col-md-4">
												<div class="text-left">
													<?php if ($res->status == 3): ?>
														Penting
													<?php elseif ($res->status == 2): ?>
														Bisa Ditunda
													<?php else: ?>
														Rencana
													<?php endif ?>
												</div>
											</div>
											<div class="col-md-8">
												<div class="text-right">
													<a href="#" class="showDetail" data-id="<?= $res->task_id?>" data-title="<?= $res->title ?>" data-description="<?= $res->description ?>" data-info="<?= $res->info ?>" data-status="<?php if ($res->status == 1) : echo "Rencana"; elseif ($res->status == 2) : echo "Bisa Ditunda"; else : echo "Penting"; endif;?>"><i class="fa fa-eye fa-fw"></i></a>
													<a href="#" class="editTask" data-id="<?= $res->task_id?>" data-title="<?= $res->title ?>" data-description="<?= $res->description ?>" data-info="<?= $res->info ?>"><i class="fa fa-pencil fa-fw"></i></a>
													<a href="<?= base_url('task/remove/').$res->task_id ?>"><i class="fa fa-trash fa-fw"></i></a>
													<a href="<?= base_url('task/update_process/').$res->task_id ?>"><i class="fa fa-arrow-right fa-fw"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php $i++; endforeach; ?>
							<?php else: ?>
								<p class="text-center">Tidak Ada Data</p>
							<?php endif;?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Content 2 -->
			<div class="col-md-4">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<div class="text-center">
							<h4>Proses Tugas</h4>
						</div>
						<div class="panel-body">
							<div class="row">
								<?php if ($resultProcess == true) : ?>
								<?php $i = 1; foreach($resultProcess as $res): ?>
								<div class="panel panel-default">
									<a href="javascript:;">
										<div class="panel-body panel-toggle-list" id="panel-toggle-pro-<?= $res->task_id?>">
											<?= $i ?>.
											<?= $res->title?>
											/ <?= $res->description ?>
											/ <?= $res->info ?>
										</div>
									</a>
									<div class="panel-footer panel-toggle-content-list" id="panel-toggle-content-pro-<?= $res->task_id?>">
										<div class="row">
											<div class="col-md-4">
												<div class="text-left">
													<?php if ($res->status == 3): ?>
														Penting
													<?php elseif ($res->status == 2): ?>
														Bisa Ditunda
													<?php else: ?>
														Rencana
													<?php endif ?>
												</div>
											</div>
											<div class="col-md-8">
												<div class="text-right">
													<a href="<?= base_url('task/update_list/').$res->task_id ?>"><i class="fa fa-arrow-left fa-fw"></i></a>
													<a href="#" class="showDetail" data-id="<?= $res->task_id?>" data-title="<?= $res->title ?>" data-description="<?= $res->description ?>" data-info="<?= $res->info ?>" data-status="<?php if ($res->status == 1) : echo "Rencana"; elseif ($res->status == 2) : echo "Bisa Ditunda"; else : echo "Penting"; endif;?>"><i class="fa fa-eye fa-fw"></i></a>
													<a href="#" class="editTask" data-id="<?= $res->task_id?>" data-title="<?= $res->title ?>" data-description="<?= $res->description ?>" data-info="<?= $res->info ?>"><i class="fa fa-pencil fa-fw"></i></a>
													<a href="<?= base_url('task/remove/').$res->task_id ?>"><i class="fa fa-trash fa-fw"></i></a>
													<a href="<?= base_url('task/update_done/').$res->task_id ?>"><i class="fa fa-arrow-right fa-fw"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php $i++; endforeach; ?>
							<?php else: ?>
								<p class="text-center">Tidak Ada Data</p>
							<?php endif;?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Content 3 -->
			<div class="col-md-4">
				<div class="panel panel-success">
					<div class="panel-heading">
						<div class="text-center">
							<h4>Selesai</h4>
						</div>
						<div class="panel-body">
							<div class="row">
								<?php if ($resultDone == true) : ?>
								<?php $i = 1; foreach($resultDone as $res): ?>
								<div class="panel panel-default">
									<a href="javascript:;">
										<div class="panel-body panel-toggle-list" id="panel-toggle-done-<?= $res->task_id?>">
											<?= $i ?>.
											<?= $res->title?>
											/ <?= $res->description ?>
											/ <?= $res->info ?>
										</div>
									</a>
									<div class="panel-footer panel-toggle-content-list" id="panel-toggle-content-done-<?= $res->task_id?>">
										<div class="row">
											<div class="col-md-4">
												<div class="text-left">
													<?php if ($res->status == 3): ?>
														Penting
													<?php elseif ($res->status == 2): ?>
														Bisa Ditunda
													<?php else: ?>
														Rencana
													<?php endif ?>
												</div>
											</div>
											<div class="col-md-8">
												<div class="text-right">
													<a href="#" class="showDetail" data-id="<?= $res->task_id?>" data-title="<?= $res->title ?>" data-description="<?= $res->description ?>" data-info="<?= $res->info ?>" data-status="<?php if ($res->status == 1) : echo "Rencana"; elseif ($res->status == 2) : echo "Bisa Ditunda"; else : echo "Penting"; endif;?>"><i class="fa fa-eye fa-fw"></i></a>
													<a href="#" class="editTask" data-id="<?= $res->task_id?>" data-title="<?= $res->title ?>" data-description="<?= $res->description ?>" data-info="<?= $res->info ?>"><i class="fa fa-pencil fa-fw"></i></a>
													<a href="<?= base_url('task/remove/').$res->task_id ?>"><i class="fa fa-trash fa-fw"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php $i++; endforeach; ?>
							<?php else: ?>
								<p class="text-center">Tidak Ada Data</p>
							<?php endif;?>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section id="footer">
	<p class="text-center navbar-fixed-bottom">PeeM v1.0</p>
</section>

<!-- Create Task Modal -->
<div class="modal fade" id="createTask" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form action="<?= base_url('task/create') ?>" method="post">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Tambah Tugas</h4>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
	        	<label for="Judul">Judul <span class="text-danger">*</span></label>
	        	<input type="text" class="form-control" placeholder="Judul Tugas" name="title" required>
	        </div>
	        <div class="form-group">
	        	<label for="Deskripsi">Module <span class="text-danger">*</span></label>
	        	<input type="text" class="form-control" placeholder="Module Tugas" name="description" required>
	        </div>
	        <div class="form-group">
	        	<label for="Informasi">Informasi <span class="text-danger">*</span></label>
	        	<textarea name="info" placeholder="Informasi" class="form-control" required></textarea>
	        </div>
	        <div class="form-group">
	        	<label for="Status">Status <span class="text-danger">*</span></label>
	        	<select name="status" class="form-control" required>
	        		<option value="">-- Pilih Status --</option>
	        		<option value="1">Rencana</option>
	        		<option value="2">Bisa Ditunda</option>
	        		<option value="3">Penting</option>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<a href="#" onclick="showOption()">Selanjutnya</a>
	        </div>
	        <div class="next" hidden>
		        <div class="form-group">
		        	<label for="Direktori">Direktori</label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" name="dir" value="0">
		        </div>
	        	<div class="form-group">
	        	<!-- <i class="text-muted">(.html,.php,.asp,.js,.css)</i> -->
		        	<label for="File">Berkas  </label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" name="file" value="0">
		        </div>
		        <div class="form-group">
		        	<label for="Direktori">Durasi</label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" name="duration" value="0">
		        </div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
	        <button type="submit" class="btn btn-primary">Tambah</button>
	      </div>
		</form>
    </div>
  </div>
</div>

<!-- Update Task Modal -->
<div class="modal fade" id="updateTask" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<?= form_open('','id="form"') ?>
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Ubah Tugas</h4>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
	        	<label for="Judul">Judul <span class="text-danger">*</span></label>
	        	<input type="text" class="form-control" placeholder="Judul Tugas" id="title" name="title" required>
	        </div>
	        <div class="form-group">
	        	<label for="Deskripsi">Module <span class="text-danger">*</span></label>
	        	<input type="text" class="form-control" placeholder="Module Tugas" id="description" name="description" required>
	        </div>
	        <div class="form-group">
	        	<label for="Informasi">Informasi <span class="text-danger">*</span></label>
	        	<textarea id="info" name="info" placeholder="Informasi" class="form-control" required></textarea>
	        </div>
	        <div class="form-group">
	        	<label for="Status">Status <span class="text-danger">*</span></label>
	        	<select id="status" name="status" class="form-control" required>
	        		<option value="">-- Pilih Status --</option>
	        		<option value="1">Rencana</option>
	        		<option value="2">Bisa Ditunda</option>
	        		<option value="3">Penting</option>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<a href="#" onclick="showOption()">Selanjutnya</a>
	        </div>
	        <div class="next" hidden>
		        <div class="form-group">
		        	<label for="Direktori">Direktori</label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" name="dir" value="0">
		        </div>
	        	<div class="form-group">
	        	<!-- <i class="text-muted">(.html,.php,.asp,.js,.css)</i> -->
		        	<label for="File">Berkas  </label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" name="file" value="0">
		        </div>
		        <div class="form-group">
		        	<label for="Direktori">Durasi</label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" name="duration" value="0">
		        </div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
	        <button type="submit" class="btn btn-primary">Perbarui</button>
	      </div>
		<?= form_close() ?>
    </div>
  </div>
</div>

<!-- Detail Modal -->
<div class="modal fade" id="modalShow" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form action="<?= base_url('task/create') ?>" method="post">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Detail Tugas</h4>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
	        	<label for="Judul">Judul </label>
	        	<input type="text" class="form-control" placeholder="Judul Tugas" id="title" name="title" required>
	        </div>
	        <div class="form-group">
	        	<label for="Deskripsi">Module </label>
	        	<input type="text" class="form-control" placeholder="Module Tugas" id="description" name="description" required>
	        </div>
	        <div class="form-group">
	        	<label for="Informasi">Informasi </label>
	        	<textarea id="info" name="info" placeholder="Informasi" class="form-control" required></textarea>
	        </div>
	        <div class="form-group">
	        	<label for="Status">Status </label>
	        	<select name="status" class="form-control" required>
	        		<option value="" id="status"></option>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<a href="#" onclick="showOption()">Selanjutnya</a>
	        </div>
	        <div class="next" hidden>
		        <div class="form-group">
		        	<label for="Direktori">Direktori</label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" id="dir" name="dir" value="0">
		        </div>
	        	<div class="form-group">
	        	<!-- <i class="text-muted">(.html,.php,.asp,.js,.css)</i> -->
		        	<label for="File">Berkas  </label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" id="file" name="file" value="0">
		        </div>
		        <div class="form-group">
		        	<label for="Direktori">Durasi</label>
		        	<input type="text" class="form-control" placeholder="Direktori Tugas" id="duration" name="duration" value="0">
		        </div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	      </div>
		</form>
    </div>
  </div>
</div>


<script>
		function showModal(){
			$('#createTask').modal('show');
		}
		function editModal(){
			$('#updateTask').modal('show');
		}
		
		function showOption(){
			$('.next').slideToggle();
		}
		<?php $i = 1; foreach($resultList as $res): ?>
				$('#panel-toggle-content-list-<?= $res->task_id ?>').css('display','none');
			$('#panel-toggle-list-<?= $res->task_id ?>').click(function(){
				$('#panel-toggle-content-list-<?= $res->task_id ?>').slideToggle();
			});
		<?php $i++; endforeach; ?>
		<?php $i = 1; foreach($resultProcess as $res): ?>
				$('#panel-toggle-content-pro-<?= $res->task_id ?>').css('display','none');
			$('#panel-toggle-pro-<?= $res->task_id ?>').click(function(){
				$('#panel-toggle-content-pro-<?= $res->task_id ?>').slideToggle();
			});
		<?php $i++; endforeach; ?>
		<?php $i = 1; foreach($resultDone as $res): ?>
				$('#panel-toggle-content-done-<?= $res->task_id ?>').css('display','none');
			$('#panel-toggle-done-<?= $res->task_id ?>').click(function(){
				$('#panel-toggle-content-done-<?= $res->task_id ?>').slideToggle();
			});
		<?php $i++; endforeach; ?>

		$('.closeAll').click(function(){
			$('.panel-toggle-content-list').slideUp();
		});
		$('.showAll').click(function(){
			$('.panel-toggle-content-list').slideDown();
		});
		// $('.panel-toggle-list').each(function() {
		//     var strDesc = $(this).html();
		//       if ( $(this).html().length > 98) {
		//         $(this).html(strDesc.substring(0,98)+'...');
		//       };
		// });

		// $('.edit').click(function() {
	 //      var formUri = $(this).data("id");
	 //      $("#modalEdit #form").attr('action','add-stock/edit/'+formUri);

	 //      var name = $(this).data("name");
	 //      $("#modalEdit #name_menu").html(name);

	 //      var stock = $(this).data("stock");
	 //      $("#modalEdit #stock").val(stock);

	 //      var expire = $(this).data("expire");
	 //      $("#modalEdit #expire").val(expire);

	 //      $("#modalEdit").modal("show");
	 //    });
	 	$('.showDetail').click(function() {
	      var id = $(this).data("id");
	      $("#modalShow #name_menu").html(id);

	      var title = $(this).data("title");
	      $("#modalShow #title").val(title);

	      var description = $(this).data("description");
	      $("#modalShow #description").val(description);

	      var info = $(this).data("info");
	      $("#modalShow #info").val(info);

	      var status = $(this).data("status");
	      $("#modalShow #status").text(status);

	      $("#modalShow").modal("show");
	    });
	    $('.editTask').click(function() {
	      var formUri = $(this).data("id");
          $("#updateTask #form").attr('action','task/update/'+formUri);

	      var id = $(this).data("id");
	      $("#updateTask #name_menu").html(id);

	      var title = $(this).data("title");
	      $("#updateTask #title").val(title);

	      var description = $(this).data("description");
	      $("#updateTask #description").val(description);

	      var info = $(this).data("info");
	      $("#updateTask #info").val(info);

	      var status = $(this).data("status");
	      $("#updateTask #status").text(status);

	      $("#updateTask").modal("show");
	    });
</script>