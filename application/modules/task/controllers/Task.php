<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends CI_Controller {

	// Construct
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('task_model');
	  }
	
	// Index
	public function index()
	{
		$data = array(
			'resultList' => $this->task_model->getList(),
			'resultProcess' => $this->task_model->getProcess(),
			'resultDone' => $this->task_model->getDone(),
		);
		$this->load->view('template/header');
		$this->load->view('task_view',$data);
		$this->load->view('template/footer');
	}

	public function detail($id=null){
		$this->task_model->getDetail($id);
	}

	public function update($id=null){
		$data = array(
	      'title' => $this->input->post('title'),
          'description' => $this->input->post('description'),
          'info' => $this->input->post('info'),
          'directory' => $this->input->post('dir'),
          'module' => $this->input->post('file'),
          'status' => $this->input->post('status'),
          'duration' => $this->input->post('duration'),
	      'created_date' => date('Y-m-d h:i:s'),
	    );
    	$this->task_model->updateTask($data,$id);
          	redirect();
	}

	// Operations
	public function create()
	{
	    if ($_POST) {
	        $post = [
	          'title' => $this->input->post('title'),
	          'description' => $this->input->post('description'),
	          'info' => $this->input->post('info'),
	          'directory' => $this->input->post('dir'),
	          'module' => $this->input->post('file'),
	          'status' => $this->input->post('status'),
	          'duration' => $this->input->post('duration'),
	          'created_date' => date('Y-m-d h:i:s'),
	        ];
          	$this->task_model->insert($post);
          	redirect();
	    }
    }
    public function update_process($id=null)
    {
    	$this->task_model->update_process($id);
    	redirect();
    }
    public function update_list($id=null)
    {
    	$this->task_model->update_list($id);
    	redirect();
    }
    public function update_done($id=null)
    {
    	$this->task_model->update_done($id);
    	redirect();
    }
    public function remove($id=null)
    {
    	$this->task_model->remove($id);
    	redirect();
    }
}
