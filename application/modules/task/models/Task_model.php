<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Task_model extends CI_Model{

	// Get data
	function getList(){
		$this->db->where('position','0');
		$this->db->order_by('task_id', 'DESC');
		return $this->db->get('task')->result();
	}
	function getProcess(){
		$this->db->where('position','1');
		return $this->db->get('task')->result();
	}
	function getDone(){
		$this->db->where('position','2');
		return $this->db->get('task')->result();
	}
	function getDetail($id){
		$this->db->where('task_id', $id);
		return $this->db->get('task')->rows();
	}

	// Operations
	function insert($data){
		return $this->db->insert('task',$data);
	}
	function update_process($id){
		$sql = "UPDATE task SET position = 1 WHERE task_id = $id";
		return $this->db->query($sql,array($id));
	}
	function update_list($id){
		$sql = "UPDATE task SET position = 0 WHERE task_id = $id";
		return $this->db->query($sql,array($id));
	}
	function update_done($id){
		$sql = "UPDATE task SET position = 2 WHERE task_id = $id";
		return $this->db->query($sql,array($id));
	}
	function updateTask($data,$id){
		$this->db->where('task_id',$id);
   		 return $this->db->update('task',$data);
	}
	function remove($id){
		$sql = "UPDATE task SET position = 3 WHERE task_id = $id";
		return $this->db->query($sql,array($id));
	}
}