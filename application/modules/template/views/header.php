<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title></title>
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css">

		<script src="<?= base_url() ?>assets/js/jquery.js"></script>
		<script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
	</head>
	<body style="background-color: #eee">
		